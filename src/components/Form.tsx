import React, { useState } from 'react'
import { TextField, Button } from '@material-ui/core'

interface FormProps {
    onSubmit: (text: string) => void
}
export default function Form({ onSubmit }: FormProps) {
    const [text, setText] = useState('')

    function submit(): void {
        onSubmit(text)
        setText('')
    }

    const handleChange = (e: any) => {
        setText(e.target.value)
    }

    return (
        <div
            style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
            }}
        >
            <TextField
                label="Add item"
                variant="outlined"
                value={text}
                onChange={handleChange}
            />
            <Button type="submit" onClick={submit}>
                Add
            </Button>
        </div>
    )
}
