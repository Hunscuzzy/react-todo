import React, { useState } from 'react'
import { ThemeProvider } from '@material-ui/core/styles'
import { CssBaseline, makeStyles } from '@material-ui/core'
import Container from '@material-ui/core/Container'
import Paper from '@material-ui/core/Paper'
import uuid from 'uuid'

import theme from '../theme'
import Header from './Header'
import TodoList from './TodoList'
import Form from './Form'
import { TodoListItem } from './TodoItem'

const todoList: Array<TodoListItem> = [
    {
        id: uuid(),
        text: 'Do that',
        completed: false,
    },
    {
        id: uuid(),
        text: 'Do this',
        completed: true,
    },
]

const useStyles = makeStyles(t => ({
    container: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        minHeight: '100vh',
        textAlign: 'center',
    },
    paper: {
        padding: t.spacing(4),
        width: `100%`,
    },
}))

export default function App() {
    const classes = useStyles()

    const [list, setList] = useState(todoList)
    const [hideCompleted, setHideCompleted] = useState(false)

    function handleSubmit(value: string) {
        setList([
            ...list,
            {
                id: uuid(),
                text: value,
                completed: false,
            },
        ])
    }

    function updateItem(id: string) {
        const updatedList = list.map(item => {
            if (item.id === id) {
                return { ...item, completed: !item.completed }
            } else {
                return item
            }
        })

        setList(updatedList)
    }

    return (
        <ThemeProvider theme={theme}>
            <>
                <CssBaseline />
                <Container className={classes.container} maxWidth="sm">
                    <Paper className={classes.paper}>
                        <Header
                            list={list}
                            onClear={() => setList([])}
                            onHideCompleted={() =>
                                setHideCompleted(!hideCompleted)
                            }
                            hideCompleted={hideCompleted}
                        />
                        <TodoList
                            hideCompleted={hideCompleted}
                            list={list}
                            setCompleted={id => updateItem(id)}
                        />
                        <Form onSubmit={value => handleSubmit(value)} />
                    </Paper>
                </Container>
            </>
        </ThemeProvider>
    )
}
