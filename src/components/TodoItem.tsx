import React from 'react'
import { Typography, Button } from '@material-ui/core'

export interface TodoListItem {
    text: string
    completed: boolean
    id: string
}

interface TodoItemProps extends TodoListItem {
    onComplete: (id: string) => void
}

export default function TodoItem({
    completed,
    text,
    id,
    onComplete,
}: TodoItemProps) {
    const setCompleted = () => {
        onComplete(id)
    }
    return (
        <div
            style={{
                display: 'flex',
                alignItems: 'center',
            }}
        >
            <Button onClick={setCompleted}>X</Button>
            <Typography
                style={{
                    textDecoration: completed ? 'line-through' : 'none',
                }}
                align="left"
            >
                {text}
            </Typography>
        </div>
    )
}
