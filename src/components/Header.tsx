import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import LinearProgress from '@material-ui/core/LinearProgress'

import { TodoListItem } from './TodoItem'

const useStyles = makeStyles(theme => ({
    grid: {
        marginBottom: theme.spacing(4),
    },
}))

interface HeaderProps {
    hideCompleted: boolean
    onHideCompleted: () => void
    onClear: () => void
    list: TodoListItem[]
}

export default function Header({
    onHideCompleted,
    onClear,
    hideCompleted,
    list,
}: HeaderProps) {
    const classes = useStyles()

    const completedItems = list.filter(item => item.completed)

    const percent = completedItems
        ? (completedItems.length * 100) / list.length
        : 0

    return (
        <>
            <Typography component="h1" variant="h2" gutterBottom>
                To do list
            </Typography>
            <Grid container spacing={2} className={classes.grid}>
                <Grid item>
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={onHideCompleted}
                    >
                        {hideCompleted
                            ? 'Show completed items'
                            : 'Hide completed items'}
                    </Button>
                </Grid>
                <Grid item>
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={onClear}
                        disabled={list.length === 0}
                    >
                        Clear all
                    </Button>
                </Grid>
            </Grid>
            <LinearProgress variant="determinate" value={percent} />
        </>
    )
}
