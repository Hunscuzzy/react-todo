import React from 'react'

import TodoItem, { TodoListItem } from './TodoItem'

interface TodoListProps {
    list: TodoListItem[]
    setCompleted: (id: string) => void
    hideCompleted: boolean
}

export default function TodoList({
    list,
    setCompleted,
    hideCompleted,
}: TodoListProps) {
    if (hideCompleted) {
        return (
            <div>
                {list.map(item => {
                    return !item.completed ? (
                        <TodoItem
                            key={item.id}
                            onComplete={id => setCompleted(id)}
                            {...item}
                        />
                    ) : null
                })}
            </div>
        )
    }

    return (
        <div>
            {list.map(item => (
                <TodoItem
                    key={item.id}
                    onComplete={id => setCompleted(id)}
                    {...item}
                />
            ))}
        </div>
    )
}
